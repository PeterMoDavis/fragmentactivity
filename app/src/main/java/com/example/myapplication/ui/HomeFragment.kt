package com.example.myapplication.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentHomeBinding

class HomeFragment : Fragment(R.layout.fragment_home) {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentHomeBinding.inflate(inflater, container, false)
        _binding = binding
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.cvRed.setOnClickListener{
                findNavController().navigate(R.id.action_homeFragment2_to_redFragment)
        }
        binding.cvYellow.setOnClickListener{
            findNavController().navigate(R.id.action_homeFragment2_to_yellowFragment)
        }
        binding.cvPurple.setOnClickListener{
            findNavController().navigate(R.id.action_homeFragment2_to_purpleFragment)
        }
        binding.cvGreen.setOnClickListener{
            findNavController().navigate(R.id.action_homeFragment2_to_greenFragment)
        }
        binding.cvGray.setOnClickListener{
            findNavController().navigate(R.id.action_homeFragment2_to_grayFragment)
        }
        binding.cvBlue.setOnClickListener{
            findNavController().navigate(R.id.action_homeFragment2_to_blueFragment)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}